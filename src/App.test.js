/**--------------------------------------------------
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for App component
 * 
*/

import { render } from "@testing-library/react";
import React from "react";
import App from "./App";

jest.mock("./components/loginwithfb&google/login.component", () => () => "an element");
jest.mock("./components/layout/dashboard.component", () => () => "an element");

test("should load App component",()=>{
    render(<App />)
})