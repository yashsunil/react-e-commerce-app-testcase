import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Login from "./components/loginwithfb&google/login.component";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Dashboard from "./components/layout/dashboard.component";
//import AddUser from './components/adduser.component';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="auth-wrapper">
          <div className="auth-inner">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/dashboard" component={Dashboard} />
              {/* <Route component={() => (<div>404 Not found </div>)} /> */}
            </Switch>
          </div>
        </div>
      </div>
      {/* < AddUser /> */}
    </BrowserRouter>
  );
}

export default App;
