import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { ProductCard } from "./productcard.component";

configure({adapter: new Adapter()});

// Initial component load
test('component should load', () => {
    expect(<ProductCard/>).toBeTruthy();
});

// Snapshot match for post component
it('should match the snapshot', () => {
    const container = shallow(<ProductCard />);
    expect(container.html()).toMatchSnapshot();
});