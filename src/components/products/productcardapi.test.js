/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Productcardapi component
 * 
*/
import React from "react";
import { shallow, configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Productcardapi from "./productcardapi.component";
import {Route, MemoryRouter} from 'react-router-dom';

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Productcardapi />).toBeTruthy();
});

//--test Productcardapi component with Router
it('Productcardapi component should get userParams', () => {
    mount(
        <MemoryRouter initialEntries={['/products/1']}>
            <Route exact path="/products/:id" component={Productcardapi} />
        </MemoryRouter>
      );
});


