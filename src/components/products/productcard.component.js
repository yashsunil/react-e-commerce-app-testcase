import "../../css/productcard.css";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import React from "react";
import ProductCardGet from "./productcardapi.component";

export function ProductCard({ productcard }) {
  return (
    <div className="main">
      <div className="container">
        <hr className="hrproductcard" />

        <div className="productcard">
          <div className="d-flex ">
            <div className="image">
              <TransformWrapper
                initialScale={1}
                initialPositionX={0}
                initialPositionY={0}
              >
                {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                  <React.Fragment>
                    {/* <div className="tools">
              <button className="zoomIn"
              style={{
                color: "white",
                backgroundColor: " #68aaf8",
                borderColor: "#68aaf8",
                height : "5vh",
                width : "2vw",
                borderRadius: "10px",
               }}
              onClick={() => zoomIn()}>+</button>
              <button className="zoomOut"
                    style={{
                      color: "white",
                      backgroundColor: " #68aaf8",
                      borderColor: " #68aaf8",
                      height : "5vh",
                      width : "2vw",
                      borderRadius: "10px",
                      
                    }}

              onClick={() => zoomOut()}>-</button>

              <button className="zoomReset"
               style={{
                color: "white",
                backgroundColor: "#68aaf8",
                borderColor: "#68aaf8",
                height : "5vh",
                width : "2vw",
                borderRadius: "10px"
              }} 
              onClick={() => resetTransform()}>x</button>
            </div> */}
                    <TransformComponent>
                      <img
                        src={productcard?.image}
                        alt="product_img"
                        className="roundedp"
                      />
                    </TransformComponent>
                  </React.Fragment>
                )}
              </TransformWrapper>
            </div>
            <div className="ml-3 w-100">
              <button
                className="btncatp"
                data-testid="btn-category"
                id="categoryBtn"
              >
                {productcard?.category}
              </button>
              <br />

              <p className="mb-0 mt-0 textLeft titlep">{productcard?.title}</p>

              <p className="mb-0 mt-1 textLeft parap">
                {productcard?.description}
              </p>

              <p className=" textLeft pricep ">
                ${productcard?.price - 5.0}{" "}
                <small className="small">
                  <s>$ {productcard?.price}</s>
                </small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
