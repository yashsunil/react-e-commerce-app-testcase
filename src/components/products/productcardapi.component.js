import { useEffect, useState } from "react";
import { ProductCard } from "./productcard.component";
import axios from "axios";
import { useParams } from "react-router";
import '../../css/productcard.css';

const ProductCardGet = () => {

    const [card, setcard] = useState();
    const {id} = useParams()
    useEffect(() => {
        axios.get(`https://fakestoreapi.com/products/${id}`).then((response) => {
           setcard(response.data);
        });

    }, []);
    return (

        <div className="post">
            <ProductCard productcard={card}/>
        </div>
    );
};
export default ProductCardGet;