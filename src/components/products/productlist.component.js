import React, { useEffect, useState } from "react";
import "../../css/productlist.css";
import { useHistory } from "react-router";
import axios from "axios";
import { NavLink } from "react-router-dom";
import _ from "lodash";

const pageSize = 4;
const ProductListApi = () => {
  const [users, setPosts] = useState(true);
  const [paginatedPosts, setpaginatedPosts] = useState([]);
  const [currentPage, setcurrentPage] = useState(1);

  const getUsers = async () => {
    const response = await fetch("https://fakestoreapi.com/products/");
    setPosts(await response.json());
    setpaginatedPosts(_(response.data).slice(0).take(pageSize).value());
  };

  const history = useHistory();
  useEffect(() => {
    getUsers();
  }, []);

  const pageCount = users ? Math.ceil(users.length / pageSize) : 0;
  if (pageCount === 1) return null;
  const pages = _.range(1, pageCount + 1);

  const pagination = (pageNo) => {
    setcurrentPage(pageNo);
    const startIndex = (pageNo - 1) * pageSize;
    const paginatedPosts = _(users).slice(startIndex).take(pageSize).value();
    setpaginatedPosts(paginatedPosts);
  };

  const routeHandler = (id) => {
    history.push(`/dashboard/card/${id}`);
  };

  return (
    <div className="main">
      <h5 className="listprodheader">List Products</h5>
      <hr className="hr" />

      <div className="container">
        <div className="row text-center ">
          {paginatedPosts.map((currElem) => {
            return (
              <div className="col-5 col-md-6 col-sm-6 mt-5">
                <div className="card p-4 ">
                  <div className="d-flex ">
                    <div className="image">
                      <img
                        src={currElem.image}
                        alt="product_img"
                        className="rounded"
                      />
                    </div>
                    <div className="ml-3 w-100">
                      <button
                        onClick={() => {
                          routeHandler(currElem.id);
                        }}
                        className="btncat"
                        data-testid="btn-cat"
                        id="CatBtn"
                      >
                        {currElem.category}
                      </button>
                      <br />
                      <p className="mb-0 mt-0 textLeft title">
                        {currElem.title}
                      </p>
                      <p className="mb-0 mt-1 textLeft para">
                        {currElem.description}
                      </p>
                      <p className=" textLeft price ">
                        ${currElem.price - 5.0}{" "}
                        <small className="small">
                          <s>${currElem.price}</s>
                        </small>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>

        <div className="navbottom_productlist">
          <nav className="d-flex justify-content-center">
            <ul className="pagination">
              {pages.map((page) => (
                <li
                  className={
                    page === currentPage ? "page-item active" : "page-item"
                  }
                >
                  <p
                    data-testid={page}
                    className="productlists-page-link"
                    onClick={() => pagination(page)}
                  >
                    {page}
                  </p>
                </li>
              ))}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default ProductListApi;
