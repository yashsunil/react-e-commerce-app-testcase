/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Facebooklogin component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Facebooklogin from "./facebooklogin";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Facebooklogin />).toBeTruthy();
});

//--Snapshot match for Facebooklogin component
it('Facebooklogin component should match the snapshot', () => {
    const container = shallow(<Facebooklogin />);
    expect(container.html()).toMatchSnapshot();
});