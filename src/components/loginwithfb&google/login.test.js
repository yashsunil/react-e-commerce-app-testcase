/**--------------------------------------------------
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for post component
 * 
*/
import React from 'react'
import {render, waitFor, fireEvent} from '@testing-library/react'
import userEvent from '@testing-library/user-event';
import Login from './login.component';

jest.mock("./facebooklogin", () => () => "an element");
jest.mock("./loginwithgoogle", () => () => "an element");

//--Test case for login form
describe("login form should work", () => {
  test('submitting a Formik form', () => {
      const handleSubmit = jest.fn()
      const {getByText, getByTestId } = render(<Login onSubmit={handleSubmit}/>)
      expect(getByText('Yms')).toBeInTheDocument()

      userEvent.type(getByTestId("email-field"), '')
      fireEvent.blur(getByTestId("email-field"));

      userEvent.type(getByTestId("email-field"), 'test-John@abc.com')
      fireEvent.blur(getByTestId("email-field"));
      
      userEvent.type(getByTestId("password-field"), '1234')
      fireEvent.blur(getByTestId("password-field"));

      userEvent.type(getByTestId("email-field"), 'John@abc.com')
      userEvent.type(getByTestId("password-field"), '12345678')

      
      userEvent.click(getByTestId("btn-login"))    
      waitFor(() =>
          expect(handleSubmit).toHaveBeenCalledWith({
            email: 'John@abc.com',
            password: '12345678',
        },
        //--setItem called after api success
        expect(window.localStorage.setItem).toHaveBeenCalledTimes(1)
      ),
      )
    });
});

