/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Loginwithgoogle component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Loginwithgoogle from "./loginwithgoogle";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Loginwithgoogle />).toBeTruthy();
});

//--Snapshot match for Loginwithgoogle component
it('Loginwithgoogle component should match the snapshot', () => {
    const container = shallow(<Loginwithgoogle />);
    expect(container.html()).toMatchSnapshot();
});