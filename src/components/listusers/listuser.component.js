import React, {useEffect, useState} from 'react';
import _ from "lodash";
import '../../css/listuser.css'
import { useHistory } from 'react-router';
import { fetchUsers } from "../Mock/MockAPI";
const pageSize = 3;
const Posts = () =>{
    const [posts, setPosts]= useState(true);
    const [paginatedPosts, setpaginatedPosts] = useState();
    const [currentPage, setcurrentPage] = useState(1);
    const history = useHistory()
    useEffect( async ()=>{
        const res = await fetchUsers();
        setPosts(res.data);
        setpaginatedPosts(_(res.data).slice(0).take(pageSize).value());
    },[]);

    const pageCount = posts ? Math.ceil(posts.length/pageSize) : 0;
    if (pageCount === 1) return null;
    const pages = _.range(1,pageCount+1);

    const pagination = (pageNo) =>{
        setcurrentPage(pageNo);
        const startIndex = (pageNo - 1)* pageSize;
        const paginatedPosts = _(posts).slice(startIndex).take(pageSize).value();
        setpaginatedPosts(paginatedPosts)
    }
 

   const  routeHandler = (id) =>{
    history.push(`/dashboard/posts/${id}`)
    }

    return <div>
        <div>
            <h5 className="userheader">User Manager</h5><br/>
            <img className="information-icon" src="https://static.vecteezy.com/system/resources/thumbnails/000/442/530/small/Basic_Element_15-30__28566_29.jpg"/> 
            <hr className="hr"/>
        {
            !paginatedPosts ? ("No data found") : (
                <div className="usertable">
                    <table className="table table-borderless text-center">
                    <thead>
                        <tr className="h">
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">City</th>
                            <th scope="col">zipcode</th>
                            {/* <th scope="col">Map</th> */}
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        {
                            paginatedPosts.map((item, index)=>(
                                <tr key={index} onClick={()=>{routeHandler(item.id)}}>
                                    <td>{item.id}</td>
                                     <td>{item.name}</td>
                                     <td >{item.username}</td>
                                     <td>{item.email}</td>
                                     <td>{item.address.city}</td>
                                     <td>{item.address.zipcode}</td>
                                     
                                </tr>
                            ))
                        }     
                    </tbody>
                </table>
                <div className='bottom-box'></div>

                </div>
            )}
            <div className="navbottom" >
             <nav  className="d-flex justify-content-center">
                <ul className="pagination">
                    {
                    pages.map((page)=>(
                        <li className={
                            page === currentPage ? "page-item active" : "page-item"
                        }>
                        <p className="userlists-page-link" data-testid={page} onClick= { ()=> pagination(page)}>{page}</p>
                        </li>
                    ))}
                </ul>
             </nav>
            </div>
        </div>
    </div>
}
export default Posts;
