import axios from "axios";

export const BASE_URL = "https://jsonplaceholder.typicode.com";

export const fetchUsers = async () => {
  try {
    var udata = await axios.get(`${BASE_URL}/users`)
    .then(res=>{ return res; });
    return udata;
  } catch (e) {
    return [];
  }
};