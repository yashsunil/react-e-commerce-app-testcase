/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for GoogleMap component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { GoogleMap } from "./googlemap";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<GoogleMap />).toBeTruthy();
});

//--Snapshot match for GoogleMap component
it('GoogleMap component should match the snapshot', () => {
    const container = shallow(<GoogleMap />);
    expect(container.html()).toMatchSnapshot();
});