/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Googlemapapi component
 * 
*/
import React from "react";
import { shallow, configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Googlemapapi from "./googlemapapi";
import {Route, MemoryRouter} from 'react-router-dom';

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Googlemapapi />).toBeTruthy();
});

//--test Googlemapapi component with Router
it('Googlemapapi component should get userParams', () => {
    const wrapper = mount(
        <MemoryRouter initialEntries={['/users/1']}>
            <Route exact path="/users/:id" component={Googlemapapi} />
        </MemoryRouter>
      );
});


