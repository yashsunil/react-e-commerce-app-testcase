import {GoogleMap} from "./googlemap";
import axios from "axios";
import { useParams } from "react-router";
import { useState, useEffect } from "react";
// import '../../css/productcard.css';

const GetMapLocation = () => {

    const [map, setmap] = useState();
    const {id} = useParams()
    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com/users/${id}`).then((response) => {
           setmap(response.data);
        });

    }, []);
    return (

        <div className="map">
            <GoogleMap googlemap = {map}/>
        </div>
    );
};
export default GetMapLocation;