/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Dashboard component
 * 
*/
import React from "react";
import Dashboard from "./dashboard.component";
import { render} from "@testing-library/react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({adapter: new Adapter()});

jest.mock("./sidebar", () => () => "an element");
jest.mock("./navbar.component", () => () => "an element");
jest.mock("../userdetails/Posts/Posts", () => () => "an element");
jest.mock("../products/productcardapi.component", () => () => "an element");
jest.mock("../googlemap/googlemapapi", () => () => "an element");
jest.mock("../shopping/Main", () => () => "an element");
jest.mock("../shopping/Cart/CheckoutForm", () => () => "an element");

jest.mock("../listusers/listuser.component", () => () => "an element");
jest.mock("../products/productlist.component", () => () => "an element");
jest.mock("../adduser/adduser.component", () => () => "an element");

//--Dashboard component should load
it('Dashboard component should load', () => {
    render(<Dashboard />)
});
