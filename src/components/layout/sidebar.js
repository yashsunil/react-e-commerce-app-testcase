import React from "react";
import { CDBSidebarContent, CDBSidebarMenu, CDBSidebarMenuItem,} from "cdbreact";
import { NavLink } from "react-router-dom";
import ReactRoundedImage from "react-rounded-image";
import MyPhoto from "../../images/pic.jpg";
import '../../css/sidebar.css';
import { useHistory } from "react-router";

const Sidebar = () => {
const history =  useHistory()
const logout =()=>{
  history.replace("/login")
  history.push("/login")
  window.location.href="/login"
  localStorage.clear();
}

  return (
    
    <div className="Sidebar">
      <div className="header">
        <ReactRoundedImage
          className="rounded-image"
          image={MyPhoto}
          roundedColor="white"
          imageWidth="30"
          imageHeight="30"
          roundedSize="6"
          borderRadius="70"
        />
       <h5 className="sidebarname" data-testid="sidebarname"> Eve Holt </h5>
      </div>

      <div
        style={{
          width: "90%",
          marginLeft: "10px",
          color: "#eaf2ff",
          marginTop: "-8px",
          marginBottom: "10px",
          backgroundColor: "#eaf2ff",
        }}
        className="row"
      >
        <hr/>
      </div>

      <CDBSidebarContent className="sidebar-content">
        <CDBSidebarMenu>
          <NavLink exact to="/dashboard" activeClassName="activeClicked">
            <CDBSidebarMenuItem className="data-list" icon="table"> 
              Dashboard
            </CDBSidebarMenuItem>
          </NavLink>
          <NavLink exact to="/products" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="list"
              style={{
                marginTop: "-12px",
                
              }}
            >
              Products
            </CDBSidebarMenuItem>
          </NavLink>

          <NavLink exact to="/dashboard/productlist" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list-list"
              style={{
                fontSize: "0.8rem",
                marginTop: "-20px",
                }}
            >
              
              <span></span>  <div className="bullet"></div>List Products
            </CDBSidebarMenuItem>
          </NavLink>

          {/* <div
            style={{
              width: "70%",
              height : "1.5px",
              marginLeft: "60px",
              color: "#eaf2ff",
              marginBottom: "10px",
              marginTop: "-20px",
              backgroundColor: "#eaf2ff",
            }}
            className="row"
          >
            <hr className = "sidebarHr"/>
          </div> */}

          <NavLink exact to="/Add users" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list-list"
              style={{
                fontSize: "0.8rem",
                marginTop: "-20px",
            
              }}
            >
              
              <span ></span><div className="bullet"></div> Add a new Product
            </CDBSidebarMenuItem>
          </NavLink>

          <NavLink exact to="/filemanager" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="file"
              style={{
                marginTop: "-12px",
              }}
            >
              File manager
            </CDBSidebarMenuItem>
          </NavLink>
          <NavLink exact to="/" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="user"
              style={{
                marginTop: "-12px",
              }}
            >
             User manager
            </CDBSidebarMenuItem>
          </NavLink>

          <NavLink exact to="/dashboard" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list-list"
              style={{
                fontSize: "0.8rem",
                marginTop: "-20px",
              
              }}
            >
              
              <span ></span> <div className="bullet"></div> List users
            </CDBSidebarMenuItem>
          </NavLink>

          {/* <div
            style={{
              width: "70%",
              height: "1px",
              marginLeft: "60px",
              color: "#eaf2ff",
              marginBottom: "10px",
              marginTop: "-20px",

              backgroundColor: "#eaf2ff",
            }}
            className="row"
          >
            <hr  className="sidebarHr"/>
          </div> */}

          <NavLink exact to="/adduser"  activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list-list"
              style={{
                fontSize: "0.8rem",
                marginTop: "-20px",
              
              }}
            >
              
              <span></span>  <div className="bullet"></div>Add users
            </CDBSidebarMenuItem>
          </NavLink>

          <NavLink exact to="/shopping" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="table"
              style={{
                marginTop: "-12px",
              }}
            >
              Shop Now
            </CDBSidebarMenuItem>
          </NavLink>

          <NavLink exact to="/messages" activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="file"
              style={{
                marginTop: "-12px",
              }}
            >
              Messages 
              <div className="dot">8</div>
            </CDBSidebarMenuItem>
          </NavLink>

          <div
            style={{
              width: "90%",
              marginLeft: "10px",
              color: "#eaf2ff",
              marginTop: "25px",
              marginBottom: "8px",

              backgroundColor: "#eaf2ff",
            }}
            className="row"
          >
            <hr />
          </div>
        
          <NavLink data-testid="logout"  to='/login' exact={true} onClick={()=>logout()} activeClassName="activeClicked">
            <CDBSidebarMenuItem
              className="data-list"
              icon="user"
              style={{
                marginTop: "-11px",
              
              }}
            >
              Logout
            </CDBSidebarMenuItem>
          </NavLink>
        </CDBSidebarMenu>
      </CDBSidebarContent>
    </div>
  );
};

export default Sidebar;
