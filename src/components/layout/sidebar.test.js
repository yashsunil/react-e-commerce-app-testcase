/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Sidebar component
 * 
*/
import React from "react";
import { fireEvent, render} from "@testing-library/react";
import { configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Sidebar from "./sidebar";
import { BrowserRouter } from "react-router-dom";
configure({adapter: new Adapter()});

//--Sidebar component should load
it('Sidebar component should load', () => {
    const { getByTestId } = render(<BrowserRouter><Sidebar /></BrowserRouter>);
    fireEvent.click(getByTestId("logout"));    
});