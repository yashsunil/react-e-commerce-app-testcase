import React, { Component } from "react";
import "../../css/navbar.css";

export default class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-default">
        <button
          className="btn-logo"
          id="LogoButton"
          data-testid="btn-logo"
          style={{
            marginTop: "0.6rem",

            marginLeft: "-18px",
            width: "4.2rem",
          }}
        >
          Yms
        </button>

        <img
          className="search-icon"
          src="https://cdn1.iconfinder.com/data/icons/web-and-mobile-ui-outline-icon-kit/512/UI_Icons_2-01-512.png"
        ></img>
        <img
          className="bell-icon"
          src="https://static.vecteezy.com/system/resources/previews/001/505/138/non_2x/notification-bell-icon-free-vector.jpg"
        ></img>
        <img
          className="user-icon"
          src="https://cdn0.iconfinder.com/data/icons/set-ui-app-android/32/8-512.png"
        ></img>
      </nav>
    );
  }
}
