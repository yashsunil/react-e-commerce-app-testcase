/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Navbar component
 * 
*/
import React from "react";
import Navbar from "./navbar.component";
import { render } from "@testing-library/react";

//--Initial component load
test('component should load', () => {
    render(<Navbar />);
});