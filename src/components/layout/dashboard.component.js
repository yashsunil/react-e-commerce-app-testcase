import React, { Component, lazy, Suspense } from 'react'
import { Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import Sidebar from './sidebar';
import '../../css/dashboard.css';
import Navbar from './navbar.component';
import Posts from '../userdetails/Posts/Posts';
import ProductCardApi from '../products/productcardapi.component';
import GetMapLocation from '../googlemap/googlemapapi';
import MainShop from '../shopping/Main';
import Checkout from '../shopping/Cart/CheckoutForm';

// Lazy Loading
const ListUsers= lazy(()=> import ('../listusers/listuser.component'));
const ProductListApi= lazy(()=> import ('../products/productlist.component'))
const AddUser= lazy(()=> import ('../adduser/adduser.component'));


export default class Dashboard extends Component {
    componentDidMount(){
        // checking availability of token from local storage
        // if token is not present it redirects to login page
        // if(!localStorage.getItem('token')){
        // this.props.history.push('/login')
        // }
    }
    render() {
        return (
            <Router>
            <div className="container">
                <div className="row">
                    <Navbar />
                    <div className="col-4"><Sidebar /></div>
                     <div className="carddiv">
                        <Suspense fallback = {<p> Loading..... </p>} >
                        <Switch>
                            <Route exact path="/dashboard" component={ListUsers} />
                            <Route exact path="/dashboard/productlist" component={ProductListApi} />
                            <Route exact path="/adduser" component={AddUser} />
                            <Route exact path="/dashboard/posts/:id" component={Posts} />
                            <Route exact path="/dashboard/card/:id" component= {ProductCardApi}/> 
                            <Route exact path="/dashboard/map/:id" component={GetMapLocation}/>
                            <Route exact path="/shopping" component={MainShop} />
                            <Route exact path="/checkoutform" component={Checkout} />
                        </Switch>
                        </Suspense>
                            </div>
                         </div>
                     </div>
            </Router>
        )
         
    }
}
 