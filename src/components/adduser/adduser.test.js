import React from "react";
import { render, screen, fireEvent } from '@testing-library/react';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallow, configure, mount } from 'enzyme';
import AddUser from './adduser.component';
import userEvent from '@testing-library/user-event';

configure({adapter: new Adapter()});
test('should renders text', () => {
    render(<AddUser/>);
    const headingElement = screen.getByText(/Add a User/i);
    expect(headingElement).toBeInTheDocument();
});

test('should renders heading', () => {
    render(<AddUser/>);
    const headingElement = screen.getByRole("heading");
    expect(headingElement).toBeInTheDocument();
});


// test('should renders "User successfully added id:" if the button was clicked', () => {
//     render(<AddUser />);
//     const buttonElement = screen.getByRole('button');
//     userEvent.click(buttonElement)
//     const outputElement = screen.getByText('User successfully added id: ');
//     expect(outputElement).toBeInTheDocument();
// });

test('renders posts if request succeeds', async () => {
    window.fetch = jest.fn();
    window.fetch.mockResolvedValueOnce({
      json: async () => [{email: 'eve.holt@reqres.in', password: 'pistol' }],
    });
    render(<AddUser />);

    const listItemElements = await screen.findAllByRole('textbox');
    expect(listItemElements).not.toHaveLength(0);
  });

  //----------Sunil---------------------------------------

  /**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for add user component
 * 
*/

  // index.test.js
// const getFirstAlbumTitle = require('./index');
// const axios = require('axios');

// jest.mock('axios');

//---Test case for preventDefault method
describe('handlesubmit method', () => {
  const component = shallow(<AddUser />) 
    test("should cover prevent default",()=>{
      let prevented = false;
      component.find("form").simulate("submit",{
        preventDefault:()=>{
          prevented = true;
        } 
      });
      expect(prevented).toBe(true);
    });

    test("loggedIn should be true",()=>{
      component.setState({
        loggedIn: true
      });
    });
});

//---Test case form email and password input should be undefiend
  describe('form input values', () => {
    const wrapper = shallow(<AddUser />);
    const mEvent = { target: { value: 'ok' } };
    it('Email value test', () => {
      wrapper.find('input#email').simulate('change', mEvent);      
    })
    it('Password value test', () => {
      wrapper.find('input#password').simulate('change', mEvent);      
    })
    expect(undefined).toEqual(undefined);
  })




