import React, { Component } from "react";
import "../../css/adduser.css";
import axios from "axios";

export default class AddUser extends Component {
  state = {};
  handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      email: this.email,
      password: this.password,
    };
    axios
      .post("register", data)
      .then((res) => {
        localStorage.setItem("token", res.data.token);
        this.setState({
          loggedIn: true,
        });
        alert("User successfully added id:" + res.data.id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="container">
        <h5 className="heading">Add a User </h5>
        <hr className="hr" />

        <form onSubmit={this.handleSubmit}>
          <div className="form-group2">
            <input
              type="email"
              className="form-control"
              placeholder="Username"
              id="email"
              onChange={(e) => (this.email = e.target.value)}
              required
              data-testid="email-field"
            />
          </div>
          <div className="form-group2">
            <input
              type="password"
              className="form-control"
              placeholder="Password"
              id="password"
              onChange={(e) => (this.password = e.target.value)}
              required
              data-testid="password-field"
            />
          </div>
          <div>
            <button
              className="addbtn"
              type="submit"
              id="addBtn"
              data-testid="add-btn"
            >
              Add User
            </button>
            <span> </span>
            <button
              className="resetbtn"
              type="reset"
              id="resetBtn"
              data-testid="reset-btn"
            >
              Reset
            </button>
          </div>
        </form>
      </div>
    );
  }
}
