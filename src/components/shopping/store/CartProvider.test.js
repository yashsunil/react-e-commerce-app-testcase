/**--------------------------------------------------
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for CartProvider component
 * 
*/

import { render } from "@testing-library/react";
import React from "react";
import CartProvider, { cartReducer } from "./CartProvider";

test("My cart should work", () => {
    render(<CartProvider />)
})

describe("cartReducer:", () => {
    const defaultCartState = {
        items: [{
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
           }],
        totalAmount: 0,
    };
    
    const emptyDefaultCartState = {
        items: [],
        totalAmount: 0,
    };

    it("ADD with Default Cart State", () => {
      const action = {
        type: 'ADD',
        item : {
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
               },        
            };
      const state = cartReducer(defaultCartState, action);
      const expectedState = {
        ...defaultCartState,
            items:[{
                'amount': 2,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
            }],
            totalAmount: 109.95,
      };
      expect(state).toEqual(expectedState);
    });
    
    it("Empty Default Cart State ADD", () => {
      const action = {
        type: 'ADD',
        item : {
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
               },        
            };
      const state = cartReducer(emptyDefaultCartState, action);
      const expectedState = {
        ...emptyDefaultCartState,
            items:[{
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
            }],
            totalAmount: 109.95,
      };
      expect(state).toEqual(expectedState);
    });

    it("Remove with Default Cart State", () => {
        const action = {
          type: 'REMOVE',
          id: 1,
          item : {
                  'amount': 1,
                  'id': 1,
                  'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                  'name': "Mens clothing",
                  'price': 109.95,
                 },        
              };
        const state = cartReducer(defaultCartState, action);
        const expectedState = {
          ...defaultCartState,
              items:[],
              totalAmount: -109.95,
        };
        expect(state).toEqual(expectedState);
    });

    it("Remove with empty expected state", () => {
        const RemoveDefaultCartState = {
            items: [{
                    'amount': 2,
                    'id': 1,
                    'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                    'name': "Mens clothing",
                    'price': 109.95,
               }],
            totalAmount: 0,
        };
        const action = {
          type: 'REMOVE',
          id: 1,
          item : {
                  'amount': 1,
                  'id': 1,
                  'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                  'name': "Mens clothing",
                  'price': 109.95,
                 },        
        };
        const state = cartReducer(RemoveDefaultCartState, action);
        const expectedState = {
          ...RemoveDefaultCartState,
              items:[{
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
               }],
              totalAmount: -109.95,
        };
        expect(state).toEqual(expectedState);
    });

    it("default", () => {
        const action = {
            type: 'test',
            id: 1,
            item : {
                    'amount': 1,
                    'id': 1,
                    'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                    'name': "Mens clothing",
                    'price': 109.95,
                   },        
          };
        const state = cartReducer(emptyDefaultCartState, action);
        const expectedState = {
          ...emptyDefaultCartState,
        };
        expect(state).toEqual(expectedState);
      });
});