import { useState } from 'react';
import Header from './header/Header';
import Items from './Items/Items';
import Cart from './Cart/Cart';
import CartProvider from './store/CartProvider';

function MainShop() {
const [cartIsShown, setCartIsShown] = useState(false);

  const showCartHandler = () => {
    setCartIsShown(true);
  };

  const hideCartHandler = () => {
    setCartIsShown(false);
  };

  return (
  
    <CartProvider>
      {cartIsShown && <Cart onClose={hideCartHandler} />}
      <Header onShowCart={showCartHandler} />
      <main>
        <Items />
      </main>
    </CartProvider>
 
  );
}
export default MainShop;
