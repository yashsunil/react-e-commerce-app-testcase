/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for CheckoutForm component
 * 
*/
import React from "react";
import CheckoutForm from "./CheckoutForm";
import { render } from "@testing-library/react";
import userEvent from '@testing-library/user-event';
//--Initial component load
test('CheckoutForm component should render', () => {
    render(<CheckoutForm />)
});

//--Submit form
test('submitting a Formik form', () => {
    const {getByTestId } = render(<CheckoutForm />)    
    userEvent.click(getByTestId("confirm-btn")) 
  });