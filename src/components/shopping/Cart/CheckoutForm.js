import { useRef, useState } from "react";
import classes from "./CheckoutForm.module.css";
import StripeCheckout from "react-stripe-checkout";

const isEmpty = (value) => value.trim() === "";
const isFiveChars = (value) => value.trim().length === 5;

const Checkout = (props) => {
  const [formInputsValidity, setFormInputsValidity] = useState({
    name: true,
    street: true,
    city: true,
    postalCode: true,
  });

  const nameInputRef = useRef();
  const streetInputRef = useRef();
  const postalCodeInputRef = useRef();
  const cityInputRef = useRef();

  const confirmHandler = (event) => {
    event.preventDefault();

    const enteredName = nameInputRef.current.value;
    const enteredStreet = streetInputRef.current.value;
    const enteredPostalCode = postalCodeInputRef.current.value;
    const enteredCity = cityInputRef.current.value;

    const enteredNameIsValid = !isEmpty(enteredName);
    const enteredStreetIsValid = !isEmpty(enteredStreet);
    const enteredCityIsValid = !isEmpty(enteredCity);
    const enteredPostalCodeIsValid = isFiveChars(enteredPostalCode);

    setFormInputsValidity({
      name: enteredNameIsValid,
      street: enteredStreetIsValid,
      city: enteredCityIsValid,
      postalCode: enteredPostalCodeIsValid,
    });

    const formIsValid =
      enteredNameIsValid &&
      enteredStreetIsValid &&
      enteredCityIsValid &&
      enteredPostalCodeIsValid;

    if (!formIsValid) {
      return;
    }

    // Submit cart data
  };

  const nameControlClasses = `${classes.control} ${
    formInputsValidity.name ? "" : classes.invalid
  }`;
  const streetControlClasses = `${classes.control} ${
    formInputsValidity.street ? "" : classes.invalid
  }`;
  const postalCodeControlClasses = `${classes.control} ${
    formInputsValidity.postalCode ? "" : classes.invalid
  }`;
  const cityControlClasses = `${classes.control} ${
    formInputsValidity.city ? "" : classes.invalid
  }`;

  return (
    <div>
      {/* <h5 className="checkoutHeader">Checkout Form</h5> */}
      <form className={classes.form} onSubmit={confirmHandler}>
        <div className={nameControlClasses}>
          <input
            type="text"
            id="name"
            placeholder="First Name"
            ref={nameInputRef}
            data-testid="first-name-field"
          />
          {!formInputsValidity.name && <p>Please enter a valid first name!</p>}
        </div>
        <div className={nameControlClasses}>
          <input
            type="text"
            id="last-name"
            placeholder="Last Name"
            ref={nameInputRef}
            data-testid="last-name-field"
          />
          {!formInputsValidity.name && <p>Please enter a valid last name!</p>}
        </div>
        <div className={streetControlClasses}>
          <input
            type="text"
            id="street"
            placeholder="Address"
            ref={streetInputRef}
            data-testid="street-field"
          />
          {!formInputsValidity.street && <p>Please enter a valid street!</p>}
        </div>
        <div className={postalCodeControlClasses}>
          <input
            type="text"
            id="postal"
            placeholder="Postal Code"
            ref={postalCodeInputRef}
            data-testid="postal-code-field"
          />
          {!formInputsValidity.postalCode && (
            <p>Please enter a valid postal code (5 characters long)!</p>
          )}
        </div>
        <div className={cityControlClasses}>
          <input
            type="text"
            id="city"
            placeholder="City"
            ref={cityInputRef}
            data-testid="city-field"
          />
          {!formInputsValidity.city && <p>Please enter a valid city!</p>}
        </div>
        <div className={classes.actions}>
          <button
            type="button"
            onClick={props.onCancel}
            data-testid="cancel-btn"
            id="CancelBtn"
          >
            Cancel
          </button>
          <button
            className={classes.submit}
            data-testid="confirm-btn"
            id="ConfirmBtn"
          >
            Confirm
          </button>
        </div>
      </form>
    </div>
  );
};

export default Checkout;
