/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for CartItem component
 * 
*/
import React from "react";
import CartItem from "./CartItem";
import { render } from "@testing-library/react";

//--Initial component load
test('CartItem component should render', () => {
    const props = {
        'amount': 1,
        'id': 1,
        'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
        'name': "Mens clothing",
        'price': 109.95,
    };
    render(<CartItem {...props}/>)
});