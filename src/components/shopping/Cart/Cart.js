import { useContext } from "react";
import { Router } from "react-router-dom";
import { NavLink } from "react-router-dom";
import Modal from "../UI/Modal";
import CartItem from "./CartItem";
import classes from "./Cart.module.css";
import CartContext from "../store/cart-context";
import StripeCheckout from "react-stripe-checkout";

const Cart = (props) => {
  const cartCtx = useContext(CartContext);

  const totalAmount = `$${cartCtx.totalAmount.toFixed(2)}`;
  const hasItems = cartCtx.items.length > 0;

  const cartItemRemoveHandler = (id) => {
    cartCtx.removeItem(id);
  };

  const cartItemAddHandler = (item) => {
    cartCtx.addItem({ ...item, amount: 1 });
  };

  const cartItems = (
    <ul className={classes["cart-items"]}>
      {cartCtx.items.map((item) => (
        <CartItem
          key={item.id}
          name={item.name}
          amount={item.amount}
          price={item.price}
          image={item.image}
          onRemove={cartItemRemoveHandler.bind(null, item.id)}
          onAdd={cartItemAddHandler.bind(null, item)}
        />
      ))}
    </ul>
  );

  function handleToken(token, addresses) {
    console.log({ token, addresses });
  }

  return (
    // <div className='Cart'>
    <Modal onClose={props.onClose}>
      {cartItems}
      <div className={classes.total}>
        <span>Total Amount</span>
        <span>{totalAmount}</span>
      </div>
      <div className={classes.actions}>
        <button
          className={classes["button--alt"]}
          onClick={props.onClose}
          id="CloseBtn"
          data-testid="close-btn"
        >
          Close
        </button>
        {/* {hasItems && <button className={classes.button}>Proceed to Buy</button>}   */}
        <StripeCheckout
          stripeKey="pk_test_51K68coSDy7jyONeFK54kjAb8yy0K7lwrXmw6G0Mr9ndgRkqnioofHkgvnL0KhrRSwwcuzp4QfAqhtF4e3yg4tlLo00pFAOaXWC"
          token={handleToken}
        />
      </div>
    </Modal>
    // </div>
  );
};

export default Cart;
