import classes from "./CartItem.module.css";

const CartItem = (props) => {
  const price = `$${props.price.toFixed(2)}`;

  return (
    <li className={classes["cart-item"]}>
      <div>
        <h2>{props.name}</h2>
        <div className={classes.summary}>
          <span className={classes.image}>
            <img src={props.image} width="40px" />
          </span>
          <span className={classes.price}>{price}</span>

          <span className={classes.amount}>x {props.amount}</span>
        </div>
      </div>
      <div className={classes.actions}>
        <button
          onClick={props.onRemove}
          data-testid="remove-btn"
          id="removeBtn"
        >
          -
        </button>
        <button onClick={props.onAdd} data-testid="add-btn" id="addBtn">
          +
        </button>
      </div>
    </li>
  );
};

export default CartItem;
