/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Card component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Card from "./Card";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Card />).toBeTruthy();
});

//--Snapshot match for Card component
it('Card component should match the snapshot', () => {
    const container = shallow(<Card />);
    expect(container.html()).toMatchSnapshot();
});