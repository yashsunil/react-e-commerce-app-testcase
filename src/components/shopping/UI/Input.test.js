/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Input component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import { render } from "@testing-library/react";
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Input from "./Input";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load with props', () => {
    const props = {
        "className":"input",
        "ref": jest.fn(),
        "label":"Quantity",
        "input":{
          "id": "amount_" + 1,
          "type": "number",
          "min": "1",
          "max": "5",
          "step": "1",
          "defaultValue": "1",
        },
        "data-testid":"input-btn",
        "id":"inputBtn",
    };
    render(<Input {...props}/>)
});