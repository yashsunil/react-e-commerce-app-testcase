import { Fragment } from 'react';
import ReactDOM from 'react-dom';
import classes from './Modal.module.css';


const Backdrop = (props) => {
  return <div data-testid="root-modal" className={classes.backdrop} onClick={props.onClose}/>;
};

const ModalOverlay = (props) => {
  return (
    <div className={classes.modal}>

      <div className="your_cart" 
      style={{
        height: "3rem",
        width: "100%",
        borderRadius: "13px",
        marginTop:"5px",
        backgroundColor: "#84DFFF",
      }}>
        <h5>Your Cart</h5>
      </div>

      <div className={classes.content}>{props.children}</div>
    </div>
  );
};

const portalElement = document.getElementById('root');

const Modal = (props) => {
  return (
    <Fragment>
      {ReactDOM.createPortal(<Backdrop onClose={props.onClose} />, portalElement)}
      {ReactDOM.createPortal(
        <ModalOverlay>{props.children}</ModalOverlay>,
        portalElement
      )}
    </Fragment>
  );
};

export default Modal;
