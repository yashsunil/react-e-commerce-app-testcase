/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Modal component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Modal from "./Modal";
import { render, within } from '@testing-library/react'

configure({adapter: new Adapter()});


// test('appends the element when the component is mounted', () => {
//     const root = document.createElement('div')
  
//     render(<Modal root={root} />)
  
//     const { getByTestId } = within(root)
  
//     expect(root).toContainElement(getByTestId("root-modal"))
//   })

//--Initial component load
test('component should load', () => {
    expect(<Modal />).toBeTruthy();
});

//--Snapshot match for Modal component
// it('Modal component should match the snapshot', () => {
//     const container = shallow(<Modal />);
//     expect(container.html()).toMatchSnapshot();
// });

