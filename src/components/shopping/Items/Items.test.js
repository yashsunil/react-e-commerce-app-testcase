/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Items component
 * 
*/
import React from "react";
import Items from "./Items";
import { render } from "@testing-library/react";

//--Initial component load
test('Items component should render', () => {
    render(<Items />);
});