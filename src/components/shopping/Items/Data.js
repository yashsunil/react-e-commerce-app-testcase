import Card from '../UI/Card';
import MealItem from './Add/ShowItem';
import classes from './Data.module.css';
import { Scrollbars } from 'react-custom-scrollbars';

const Data = [
  {
    id: 1,
    name: 'Mens clothing',
    description: 'Your perfect pack for everyday use and walks in the forest Stash your laptop (up to 15 inches) in the padded sleeve, your everyday',
    image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
    price: 109.95,

},

{
    id: 2,
    name: 'Mens clothing',
    description: 'Slim-fitting style, contrast raglan long sleeve, three-button henley placket. And Solid stitched shirts with round neck made for durability and a great fit for casual fashion wear and diehard baseball fans. The Henley style round neckline includes a three-button placket',
    image: 'https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg',
    price: 22.3,

},

{
    id: 3,
    name: 'Mens clothing',
    description: 'great outerwear jackets for Spring/Autumn/Winter, suitable for many occasions such as working, hiking, camping, mountain/rock climbing, cycling, traveling or other outdoors. Good gift choice for you or your family member. A warm hearted love to Father, husband or son in this thanksgiving or Christmas Day',
    image: 'https://fakestoreapi.com/img/71li-ujtlUL._AC_UX679_.jpg',
    price: 55.99,

},

{
    id: 4,
    name: 'Mens Casual Slim Fit',
    description: 'The color could be slightly different between occasions Please note that body builds vary by person, therefore, detailed size information should be reviewed below on the product description.',
    image: 'https://fakestoreapi.com/img/71YXzeOuslL._AC_UY879_.jpg',
    price:  15.99,

},

{
  id: 5,
  name: 'Jewelery',
  description: 'From our Legends Collection, the Naga was inspired by the mythical water dragon that protects the oceans pearl.From our Legends Collection, the Naga was inspired by the mythical water dragon that protects the oceans pearl. Wear facing inward to be bestowed with love and abundance, or outward for protection.',
  image: 'https://fakestoreapi.com/img/71pWzhdJNwL._AC_UL640_QL65_ML3_.jpg',
  price: 695,
},

{
  id: 6,
  name: 'Jewelery',
  description:'Satisfaction Guaranteed. Return or exchange any order within 30 days. Satisfaction Guaranteed. Return or exchange any order within 30 days.Designed and sold by Hafeez Center in the United States. Satisfaction Guaranteed. Return or exchange any order within 30 days.',
  image: 'https://fakestoreapi.com/img/61sbMiUnoGL._AC_UL640_QL65_ML3_.jpg',
  price:  168,

},

{
  id : 7,
  price:9.99,
  description :'Classic Created Wedding Engagement Solitaire Diamond Promise Ring for Her. Classic Created Wedding Engagement Solitaire Diamond Promise Ring for Her. Gifts to spoil your love more for Engagement, Wedding, Anniversary, Valentines Day',
  name:"Jewelery",
  image:"https://fakestoreapi.com/img/71YAIFU48IL._AC_UL640_QL65_ML3_.jpg",
},

{
  id: 8,
  price: 10.99,
  description : "Rose Gold Plated Double Flared Tunnel Plug Earrings. Rose Gold Plated Double Flared Tunnel Plug Earrings. Made of 316L Stainless Steel",
  name: "Jewelery",
  image:"https://fakestoreapi.com/img/51UDEzMJVpL._AC_UL640_QL65_ML3_.jpg",
},

{
  id: 9,
  price: 64,
  name: "Electronics",
  description : "USB 3.0 and USB 2.0 Compatibility Fast data transfers Improve PC. Improve PC Performance High Capacity; Compatibility Formatted NTFS for Windows 10, Windows 8.1, Windows 7; Reformatting may be required for other operating systems; Compatibility may vary depending on user’s hardware configuration and operating system",
  image : "https://fakestoreapi.com/img/61IBBVJvSDL._AC_SY879_.jpg",
  },

  {
  id : 10,
  description : "SanDisk SSD PLUS 1TB Internal SSD - SATA III 6 Gb/s. Easy upgrade for faster boot up, shutdown, application load and response (As compared to 5400 RPM SATA 2.5” hard drive; Based on published specifications and internal benchmarking tests using PCMark vantage scores) Boosts burst write performance, making it ideal for typical PC workloads The perfect balance of performance and reliability Read/write speeds of up to 535MB/s/450MB/s (Based on internal testing; Performance may vary depending upon drive capacity, host device, OS and application.)",
  price : 109,
  name: "Electronics",
  image: "https://fakestoreapi.com/img/61U7T1koQqL._AC_SX679_.jpg",
},

{
  id:11,
  description:"Silicon Power 256GB SSD 3D NAND A55 SLC Cache Performance Boost SATA III 2.5",
  price: 109,
  name: "Electronics",
  image : "https://fakestoreapi.com/img/71kWymZ+c+L._AC_SX679_.jpg",
},

{
  id : 12,
  description :"WD 4TB Gaming Drive Works with Playstation 4 Portable External Hard Drive. 3D NAND flash are applied to deliver high transfer speeds Remarkable transfer speeds that enable faster bootup and improved overall system performance. The advanced SLC Cache Technology allows performance boost and longer lifespan 7mm slim design suitable for Ultrabooks and Ultra-slim notebooks. Supports TRIM command, Garbage Collection technology, RAID, and ECC (Error Checking & Correction) to provide the optimized performance and enhanced reliability.",
  price : 114,
  name : "Electronics",
  image : "https://fakestoreapi.com/img/61mtL65D4cL._AC_SX679_.jpg",
},

{
  id : 13,
  description :"Acer SB220Q bi 21.5 inches Full HD (1920 x 1080) IPS Ultra-Thin. Expand your PS4 gaming experience, Play anywhere Fast and easy, setup Sleek design with high capacity, 3-year manufacturer's limited warranty",
  price : 599,
  name :"Electronics",
  image : "https://fakestoreapi.com/img/81QpkIctqPL._AC_SX679_.jpg",
  
},
{
  id : 14,
  description :"Samsung 49-Inch CHG90 144Hz Curved Gaming Monitor (LC49HG90DMNXZA) – Super Ultrawide Screen QLED ",
  price : 999.99,
  name : "Electronics",
  image :"https://fakestoreapi.com/img/81Zt42ioCgL._AC_SX679_.jpg",

},

{
  id : 15,
  description : "BIYLACLESEN Women's 3-in-1 Snowboard Jacket Winter Coats. 49 INCH SUPER ULTRAWIDE 32:9 CURVED GAMING MONITOR with dual 27 inch screen side by side QUANTUM DOT (QLED) TECHNOLOGY, HDR support and factory calibration provides stunningly realistic and accurate color and contrast 144HZ HIGH REFRESH RATE and 1ms ultra fast response time work to eliminate motion blur, ghosting, and reduce input lag",
  price : 56.99,
  name :"Women's clothing",
  image :"https://fakestoreapi.com/img/51Y5NI-I5jL._AC_UX679_.jpg",
},

{
  id : 16,
  description : "Lock and Love Women's Removable Hooded Faux Leather Moto Biker Jacket. 100% POLYURETHANE(shell) 100% POLYESTER(lining) 75% POLYESTER 25% COTTON (SWEATER), Faux leather material for style and comfort / 2 pockets of front, 2-For-One Hooded denim style faux leather jacket, Button detail on waist / Detail stitching at sides, HAND WASH ONLY / DO NOT BLEACH / LINE DRY / DO NOT IRON",
  price : 29.95,
  name : "Women's clothing",
  image :"https://fakestoreapi.com/img/81XH0e8fefL._AC_UY879_.jpg",
},

{
  id : 17,
  description :"Rain Jacket Women Windbreaker Striped Climbing Raincoats",
  price : 39.99,
  name :"Women's clothing",
  image : "https://fakestoreapi.com/img/71HblAHs5xL._AC_UY879_-2.jpg",
},

{
  id : 18,
  description : "MBJ Women's Solid Short Sleeve Boat Neck V Lightweight perfet for trip or casual wear---Long sleeve with hooded, adjustable drawstring waist design. Button and zipper front closure raincoat, fully stripes Lined and The Raincoat has 2 side pockets are a good size to hold all kinds of things, it covers the hips, and the hood is generous but doesn't overdo it.Attached Cotton Lined Hood with Adjustable Drawstrings give it a real styled look.",
  price : 9.85,
  name : "Women's clothing",
  image :"https://fakestoreapi.com/img/71z3kpMAYsL._AC_UY879_.jpg",
},

{
  id : 19,
  description : "Opna Women's Short Sleeve Moisture. 95% RAYON 5% SPANDEX, Made in USA or Imported, Do Not Bleach, Lightweight fabric with great stretch for comfort, Ribbed on sleeves and neckline / Double stitching on bottom hem",
  price : 7.95,
  name : "Women's clothing",
  image : "https://fakestoreapi.com/img/51eg55uWmdL._AC_UX679_.jpg",
},

{
  id : 20,
  description : "DANVOUY Womens T Shirt Casual Cotton Short. 95%Cotton,5%Spandex, Features: Casual, Short Sleeve, Letter Print,V-Neck,Fashion Tees, The fabric is soft and has some stretch., Occasion: Casual/Office/Beach/School/Home/Street. Season: Spring,Summer,Autumn,Winter.",
  price : 12.99,
  name : "Women's clothing",
  image : "https://fakestoreapi.com/img/61pHAEJ4NML._AC_UX679_.jpg",
},
];

const AvailableData = () => {
  const List = Data.map((data) => (
    <MealItem
      key={data.id}
      id={data.id}
      name={data.name}
      description={data.description}
      image={data.image}
      price={data.price}
    />
  ));

  return (
    <Scrollbars className='scroll'
    style={{ width: 820, height: 500,
       }}>
    <section>
      
    
      <div className="row">
      <Card className ="col-md-6 col-sm-3">
      {List}
      </Card>
      </div>
     
    </section>
    </Scrollbars>
  );
};

export default AvailableData;
