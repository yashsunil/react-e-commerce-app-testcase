/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for ShowItem component
 * 
*/
import React from "react";
import { shallow, configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import ShowItem from "./ShowItem";
import { render } from "@testing-library/react";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    const props = {
        'amount': 1,
        'id': 1,
        'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
        'name': "Mens clothing",
        'price': 109.95,
    };
    render(<ShowItem {...props}/>)
});