/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for AddItem component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import AddItem from "./AddItem";
import { fireEvent, render } from "@testing-library/react";
configure({adapter: new Adapter()});
//--component render with props
test('Additem component should render with props', () => {
    const addToCartHandler = jest.fn(x => 42 + x);
    const { getByTestId } = render(<AddItem id={1} onAddToCart={addToCartHandler} />)
    fireEvent.click(getByTestId("add-to-cart-btn"));
});