import { useContext } from 'react';
import AddItem from './AddItem';
import classes from './ShowItem.module.css';
import CartContext from '../../store/cart-context';

const ShowItem = (props) => {
  const cartCtx = useContext(CartContext);

  const price = `$${props.price.toFixed(2)}`;

  const addToCartHandler = amount => {
    cartCtx.addItem({
      id: props.id,
      name: props.name,
      amount: amount,
      image: props.image,
      price: props.price
    });
  };

  return (
      <div className="box">
      
      <card>
      <div className="col-md-12">
        

      
        <h3 className='name'>{props.name}</h3>
        <div className={classes.description}>{props.description}</div>
        <div className="box-image"><img src={props.image} width="100px"/></div>
    
        <div className={classes.price}>{price}</div>
      </div>
      <div>
        <AddItem id={props.id} onAddToCart={addToCartHandler} />
      </div>
   </card>
   </div>
  
    
  );
};

export default ShowItem;
