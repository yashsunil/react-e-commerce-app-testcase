/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Main component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Main from "./Main";
import { render } from "@testing-library/react";

configure({adapter: new Adapter()});

//--Mock child component
jest.mock("./header/Header", () => () => "an element");
jest.mock("./Items/Items", () => () => "an element");
jest.mock("./Cart/Cart", () => () => "an element");
jest.mock("./store/CartProvider", () => () => "an element");

//--Initial component load
test('component should load', () => {
    render(<Main />);
});