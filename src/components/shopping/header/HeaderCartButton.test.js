/**--------------------------------------------------
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for HeaderCartButton component
 * 
*/

import { render, cleanup } from "@testing-library/react";
import React from "react";
import HeaderCartButton from "./HeaderCartButton";
import CartContext from "../store/cart-context";

describe("Cart", () => {
    //--When item is not empty test case
    test("My cart should work", () => {
        render(
        <CartContext.Provider value={{
            items: [{
                'amount': 1,
                'id': 1,
                'image': "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                'name': "Mens clothing",
                'price': 109.95,
            },
        ],
        }}>      
            <HeaderCartButton />    
        </CartContext.Provider>
        );        
    });
    //--When item is empty test case
    test("when item is empty", () => {
        render(
        <CartContext.Provider value={{
            items: [],
        }}>      
            <HeaderCartButton />    
        </CartContext.Provider>
        );        
    });
});
