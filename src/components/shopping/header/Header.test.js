/**--------------------------------------------------
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for Header component
 * 
*/

import { render } from "@testing-library/react";
import React from "react";
import Header from "./Header";

test("should load Header component",()=>{
    render(<Header />)
})