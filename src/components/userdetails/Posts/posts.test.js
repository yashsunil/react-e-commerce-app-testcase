/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for post component
 * 
*/
import React from "react";
import {render, waitFor, fireEvent} from '@testing-library/react'
import { shallow, configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Posts from "./Posts";
import {Route, MemoryRouter} from 'react-router-dom';

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Posts />).toBeTruthy();
});

//--test Posts component with Router
it('post component should get userParams', () => {
    const wrapper = mount(
        <MemoryRouter initialEntries={['/dashboard/posts/1']}>
            {/* <Route exact path="/:id/" component={Posts} /> */}
            <Route exact path="/dashboard/posts/:id" component={Posts} />
        </MemoryRouter>
      );
    //   wrapper.mount()
});


