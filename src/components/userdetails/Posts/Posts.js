import { useEffect, useState } from "react";
import { Post } from "../Post/Post";
import axios from "axios";
import { useParams } from "react-router";

const Posts = () => {

    const [posts, setPosts] = useState();
    const {id} = useParams()
    useEffect(() => {
        axios.get(`https://reqres.in/api/users/${id}`).then((response) => {
            setPosts(response.data.data);
        });

    }, []);
    return (

        <div className="post">
            <Post post={posts}/>
        </div>
    );
};
export default Posts;