import "../../../css/Post.css";

export function Post({ post }) {
  return (
    <div className="detailsbox">
      <h5 className="Heading">User Details</h5>
      <hr className="userdetailsHRow" />
      <div className="row">
        <div className="col-sm-3">
          <img src={post?.avatar} className="image" width="120" />
        </div>
        <div className="col-sm-9">
          <form>
            <div className="formdiv">
              <div className="form-group1">
                <label className="label">First Name</label>
                <input
                  type="lastname"
                  className="form-control"
                  placeholder={post?.first_name}
                  id="firstname"
                  data-testid="first-name-field"
                />
              </div>
              <div className="form-group1">
                <label className="label">Last Name</label>
                <input
                  type="lastname"
                  className="form-control"
                  placeholder={post?.last_name}
                  id="lastname"
                  data-testid="last-name-field"
                />
              </div>
              <div className="form-group1">
                <label className="label">Email</label>
                <input
                  type="email"
                  className="form-control"
                  placeholder={post?.email}
                  id="email"
                  data-testid="email-field"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
