/**
 * @since March 2022
 * @author Sunil Bhawsar
 * @uses Test cases for post component
 * 
*/
import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Post } from "./Post";

configure({adapter: new Adapter()});
//--Initial component load
test('component should load', () => {
    expect(<Post/>).toBeTruthy();
});

//--Snapshot match for post component
it('should match the snapshot', () => {
    const container = shallow(<Post />);
    expect(container.html()).toMatchSnapshot();
});